# Hello \<heml\>

HEML is a XML-based markup language designed for building emails. The goal is to make building emails feel as natural as building websites.

## Install the CLI
```
$ npm install heml -g
```


## Develop
```
heml develop email.heml --open
```


## Build
```
heml build email.heml
```


## Docs

* [Overview](https://heml.io/docs/getting-started/overview)
* [Usage](https://heml.io/docs/getting-started/usage)
